(function () {
    angular
        .module("GMSApp")
        .config(GMSAppConfig);
    GMSAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function GMSAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state("Home" ,{
                url : '/home',
                templateUrl: "views/home.html",
                controller : 'GroceryController',
                controllerAs : 'ctrl'
            })
            .state("Add", {
                url: "/add",
                templateUrl: "views/addgrocery.html",
                controller : 'AddGroceryCtrl',
                controllerAs : 'ctrl'
            })
            .state("Edit", {
                url: "/edit",
                templateUrl: "views/editgrocery.html",
                controller : 'EditGroceryCtrl',
                controllerAs : 'ctrl'
            })


        $urlRouterProvider.otherwise("/home");
    }

})();
