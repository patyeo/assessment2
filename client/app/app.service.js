(function(){
    angular
        .module("GMSApp")
        .service("GMSAppAPI", [
            '$http',
            GMSAppAPI
        ]);
    
    function GMSAppAPI($http){
        var self = this;

        //to query more than 1 item
        self.searchGrocery_list = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/grocery_list?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        // param request
        self.getGrocery = function(id){
            console.log(id);
            return $http.get("/api/grocery_list/" + id)
        }

        // body request
        self.updateGrocery = function(grocery){
            console.log(grocery);
            return $http.put("/api/grocery_list",grocery);
        }


        // must learn and master this
//query and params use 'get'
//body use 'post'
// in request, there are 3 ways. one is to post from body, one is to get from parameters, one is by query string
        // post by body over request
        self.addGrocery = function(grocery){
            return $http.post("/api/grocery_list", grocery);
        }
    }
})();



