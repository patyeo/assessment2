(function () {
    angular
        .module("GMSApp")
        .controller("GroceryController", GroceryController )
        .controller("AddGroceryCtrl", AddGroceryCtrl)
        .controller("EditGroceryCtrl", EditGroceryCtrl);
        
    GroceryController.$inject = ['GMSAppAPI', '$uibModal', '$document', '$rootScope', '$scope', "$state", "$http"];
    AddGroceryCtrl.$inject = ['GMSAppAPI', '$rootScope', '$scope'];
    EditGroceryCtrl.$inject = ['GMSAppAPI', 'items', '$rootScope', '$scope'];

    function EditGroceryCtrl(GMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Grocery Ctrl");
        var self = this;
        self.items = items;
        //initializeCalendar($scope);

        GMSAppAPI.getGrocery(items).then((result)=>{
           console.log(result.data);
           self.grocery_list = result.data;
           
        })

        self.saveGrocery = saveGrocery;

        function saveGrocery(){
            console.log("save grocery ...");
            console.log(self.grocery_list.brand);
            console.log(self.grocery_list.name);
            GMSAppAPI.updateGrocery(self.grocery_list).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshGroceryList');
             }).catch((error)=>{
                console.log(error);
             })
        }

    }


    function AddGroceryCtrl(GMSAppAPI, $rootScope, $scope){
        var self = this;
        self.saveGrocery = saveGrocery;

        self.status = {
            message: ""
        };

        function saveGrocery(){
            console.log("save grocery ...");
            console.log(self.grocery_list.upc12);
            console.log(self.grocery_list.brand);
            console.log(self.grocery_list.name);

            function upcValidate(){
                self.upc12 = upc12;
                if(self.upc12 == ctrl.grocery_list.upc12){                    

                }else{

                };
            }
     
            GMSAppAPI.addGrocery(self.grocery_list).then((result)=>{
                
                console.log("Add grocery -> " + result.id);
                self.status.message = "Successfully added " + self.grocery_list.brand + " " + self.grocery_list.name;
                //$state.go('home');
                //$rootScope.$broadcast('refreshGroceryListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
            
                self.status.message = "Failed to add";
             })
            
        }


    }

    
    function GroceryController(GMSAppAPI, $uibModal, $document, $rootScope, $scope, $state, $http){
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.grocery_list = [];

        self.searchGrocery_list = searchGrocery_list;
        self.addGrocery =  addGrocery;
        self.editGrocery = editGrocery;
        self.maxsize=5;
        self.totalItems=0;
        self.currentPage = 1;
        self.pageChanged = pageChanged;

    // initialize total items to 0 first. 
        self.itemsPerPage=20;
        self.currentPage=1;
        self.pageChanged = pageChanged;


        function searchAllGrocery_list(searchKeyword, orderby, itemsPerPage, currentPage){
            GMSAppAPI.searchGrocery_list(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                console.log(results);
                self.grocery_list = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems / self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });

        }
 
        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllGrocery_list(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshGroceryList",function(){
            console.log("refresh grocery list "+ self.searchKeyword);
            searchAllGrocery_list(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshGroceryListFromAdd",function(event, args){
            console.log("refresh grocery list from id "+ args.id);
            var grocery_list = [];
            grocery_list.push(args);
            self.searchKeyword = "";
            self.grocery_list = grocery_list;
        });

        function searchGrocery_list(){
            console.log("search grceries  ....");
            console.log(self.orderby);
            searchAllGrocery_list(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        }

        function addGrocery(size, parentSelector){
            console.log("post add grocery  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/addgrocery.html',
                controller: 'AddGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }
        
        function editGrocery(id, size, parentSelector){
            console.log("Edit Grocery...");
            console.log("Grocery id > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/editGrocery.html',
                controller: 'EditGroceryCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }


    }





})();