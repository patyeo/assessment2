module.exports = function(connection, Sequelize){
    // employees this have to match the mysql table
    // return Employees object is used within the JS
    var Grocery_list = connection.define('grocery', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.INTEGER(12),
            allowNull: false,
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name:{
            type: Sequelize.STRING,
            allowNull: false
        },

    }, {
        timestamps: false
    });
    return Grocery_list;
}